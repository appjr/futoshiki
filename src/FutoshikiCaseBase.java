import java.util.Vector;

public class FutoshikiCaseBase {

    Vector<RestricaoMaiorMenor> restricoesMaiorMenor = new Vector<>();
    long nAtribuicoes = 0;
    int[][] board = null;
    int [] domain;
    Vector<String> linhasBoard;


    protected void parseBoard(Vector<String> linhasBoard) {
        int ncolumns = linhasBoard.get(0).split(" ").length;
        board = new int[linhasBoard.size()][ncolumns];
        for(int nline = 0; nline<linhasBoard.size();nline++){
            String [] lineSplitted = linhasBoard.get(nline).split(" ");
            for(int ncolumn = 0;ncolumn<ncolumns;ncolumn++){
                board[nline][ncolumn] = Integer.parseInt(lineSplitted[ncolumn]);
            }
        }
    }

    protected void parseRestricoes(Vector<String> restricoesRaw) {
        for(int nrestricao = 0; nrestricao<restricoesRaw.size();nrestricao++){
            restricoesMaiorMenor.add(new RestricaoMaiorMenor(restricoesRaw.get(nrestricao)));
        }
    }

    protected boolean checkRestricoes(int linha, int coluna){
        boolean result = true;
        for(int nrestricao=0;nrestricao<restricoesMaiorMenor.size();nrestricao++){
            if(!restricoesMaiorMenor.get(nrestricao).testaRestricao(board,linha,coluna)){
                result = false;
                break;
            }
        }
        if(result){
            result=RestricaoLinhaColuna.testaRestricao(board,linha,coluna);
        }
        return result;
    }

    public void printSolution(){
        for(int linha = 0;linha<board[0].length;linha++){
            for(int coluna = 0;coluna<board[0].length;coluna++){
                System.out.print(board[linha][coluna]+" ");
            }
            System.out.println();
        }
    }

    public long getAtribuicoes(){
        return nAtribuicoes;
    }

    protected void checkNumeroAtribuicoes() throws Exception {
        nAtribuicoes++;
        if(nAtribuicoes>=1000000000){
            throw new Exception();//Se atribuicoes>10ˆ6, aborta
        }
    }
}
