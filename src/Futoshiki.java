import java.util.Vector;

public class Futoshiki {

    public static void main(String [] strings){
        if(strings.length==2) {
            String modo = strings[0];//Modo 1=Backtracking, 2=LookAhead, 3=MVR, 4=Completo Debug, 5=Debug so tempo para report
            String fileName = strings[1];
            if(modo.equals("5")) {
                doDebug(fileName,true);
            } else if (modo.equals("4")) {
                doDebug(fileName,false);
            } else if (modo.equals("3")) {
                doLookAheadMVR(fileName);
            } else if (modo.equals("2")) {
                doLookAhead(fileName);
            }  else if (modo.equals("1")) {
                doBacktracking(fileName);
            } else {
                printInstrucoes();
            }
        } else{
            printInstrucoes();
        }
    }

    private static void printInstrucoes() {
        System.out.println("use:");
        System.out.println("Futoshiki [modo] [inputfile]");
        System.out.println("modo 1=Backtracking, 2=LookAhead, 3=MVR, 4=Completo Debug, 5=Somente tempo para report");
        System.out.println("inputfile=nome do arquivo de entrada");

    }

    private static void doBacktracking(String fileName){
        Vector<FutoshikiCaseBackTracking> casesBackTracking = new InputHelper().fileToCases(fileName);
        int numberToDo = casesBackTracking.size();

        for(int ncases=0;ncases<numberToDo;ncases++){
            long tempoInicio=0;
            long tempoTotalBacktracking=0;

            try {
                tempoInicio = System.nanoTime();
                boolean resBackTracking = casesBackTracking.get(ncases).backtracking(-1, -1, 0);
                tempoTotalBacktracking = System.nanoTime()-tempoInicio;

                if (resBackTracking) {
                    System.out.println(1+ncases);
                    casesBackTracking.get(ncases).printSolution();
                    System.out.println();
                } else {
                    System.out.println("Falhou backtracking");
                }
            } catch (Exception e){
                System.out.println("Numero de atribuicoes excede limite maximo ");
            }
        }
    }

    private static void doLookAhead(String fileName){
        Vector<FutoshikiCaseLookAhead> casesLookAhead = new InputHelper().fileToCasesLA(fileName);
        int numberToDo = casesLookAhead.size();

        for(int ncases=0;ncases<numberToDo;ncases++){
            long tempoInicio=0;
            long tempoTotalLookAhead=0;

            try{
                tempoInicio = System.nanoTime();
                boolean resLookAhead = casesLookAhead.get(ncases).backtracking(-1, -1, 1);
                tempoTotalLookAhead = System.nanoTime()-tempoInicio;

                if (resLookAhead) {
                    System.out.println(1+ncases);
                    casesLookAhead.get(ncases).printSolution();
                    System.out.println();
                } else {
                    System.out.println("Falhou look ahead");
                }
            } catch (Exception e){
                System.out.println("Numero de atribuicoes excede limite maximo ");
            }
        }
    }

    private static void doLookAheadMVR(String fileName){
        Vector<FutoshikiCaseLookAheadMVR> casesLookAheadMVR = new InputHelper().fileToCasesMVR(fileName);
        int numberToDo = casesLookAheadMVR.size();

        for(int ncases=0;ncases<numberToDo;ncases++){
            long tempoInicio=0;
            long tempoTotalLookAheadMVR=0;

            try{
                tempoInicio = System.nanoTime();
                boolean resLookAhead = casesLookAheadMVR.get(ncases).backtracking(-1, -1, 1);
                tempoTotalLookAheadMVR = System.nanoTime()-tempoInicio;
                if (resLookAhead) {
                    System.out.println(1+ncases);
                    casesLookAheadMVR.get(ncases).printSolution();
                    System.out.println();
                } else {
                    System.out.println("Falhou look ahead MVR");
                }
            } catch (Exception e){
                System.out.println("Numero de atribuicoes excede limite maximo ");
            }
        }
    }

    private static void doDebug(String fileName, boolean doTemposSomente) {
        Vector<FutoshikiCaseBackTracking> casesBackTracking = new InputHelper().fileToCases(fileName);
        Vector<FutoshikiCaseLookAhead> casesLookAhead = new InputHelper().fileToCasesLA(fileName);
        Vector<FutoshikiCaseLookAheadMVR> casesLookAheadMVR = new InputHelper().fileToCasesMVR(fileName);
        int numberToDo = casesLookAhead.size();
        if(doTemposSomente) {
            System.out.println("case#;tempoTotalBacktracking;tempoTotalLookAhead;tempoTotalLookAheadMVR;#AtribuicoesBackTracking;#AtribuicoesLookAhead;#AtribuicoesMVR");
        }
        for(int ncases=0;ncases<numberToDo;ncases++){
            if(!doTemposSomente) {
                System.out.println("Original " + (1 + ncases));
                casesLookAhead.get(ncases).printSolution();
            }
            long tempoInicio=0;
            long tempoTotalBacktracking=0;
            long tempoTotalLookAhead=0;
            long tempoTotalLookAheadMVR=0;

            try {
                tempoInicio = System.nanoTime();
                boolean resBackTracking = casesBackTracking.get(ncases).backtracking(-1, -1, 0);
                tempoTotalBacktracking = System.nanoTime()-tempoInicio;

                if (resBackTracking) {
                    if(!doTemposSomente) {
                        System.out.println();
                        System.out.println("Final Result Backtracking case #" + (1 + ncases));
                        casesBackTracking.get(ncases).printSolution();
                        System.out.println("Tempo de execução backtracking (ns): " + tempoTotalBacktracking);
                        System.out.println("Número de atribuições: " + casesBackTracking.get(ncases).getAtribuicoes());
                    }
                } else {
                    System.out.println("Falhou backtracking");
                }
            } catch (Exception e){
                System.out.println("Numero de atribuicoes excede limite maximo ");
            }

            try{
                tempoInicio = System.nanoTime();
                boolean resLookAhead = casesLookAhead.get(ncases).backtracking(-1, -1, 1);
                tempoTotalLookAhead = System.nanoTime()-tempoInicio;

                if (resLookAhead) {
                    if(!doTemposSomente) {
                        System.out.println();
                        System.out.println("Final Result Look Ahead case # " + (1 + ncases));
                        casesLookAhead.get(ncases).printSolution();
                        System.out.println("Tempo de execução LookAhead (ns): " + tempoTotalLookAhead);
                        System.out.println("Número de atribuições: " + casesLookAhead.get(ncases).getAtribuicoes());
                        System.out.println();
                    }
                } else {
                    System.out.println("Falhou look ahead");
                }
            } catch (Exception e){
                System.out.println("Numero de atribuicoes excede limite maximo ");
            }

            try{
                tempoInicio = System.nanoTime();
                boolean resLookAheadMVR = casesLookAheadMVR.get(ncases).backtracking(-1, -1, 1);
                tempoTotalLookAheadMVR= System.nanoTime()-tempoInicio;

                if (resLookAheadMVR) {
                    if(!doTemposSomente) {
                        System.out.println();
                        System.out.println("Final Result Look Ahead case com MVR # " + (1 + ncases));
                        casesLookAheadMVR.get(ncases).printSolution();
                        System.out.println("Tempo de execução LookAhead com MVR(ns): " + tempoTotalLookAhead);
                        System.out.println("Número de atribuições: " + casesLookAheadMVR.get(ncases).getAtribuicoes());
                        System.out.println();
                    }
                } else {
                    System.out.println("Falhou look ahead MVR");
                }
            } catch (Exception e){
                System.out.println("Numero de atribuicoes excede limite maximo Look Ahead MVR ");
            }
            if(!doTemposSomente) {
                //print para debug
                System.out.println("Diferença de tempo (Backtracking-Lookahead): " + (tempoTotalBacktracking - tempoTotalLookAhead));
                System.out.println("Diferença de tempo (Lookahead-LookaheadMVR): " + (tempoTotalLookAhead - tempoTotalLookAheadMVR));
                System.out.println("Diferença de atribuições(Backtracking-Lookahead): " + (casesBackTracking.get(ncases).getAtribuicoes() - casesLookAhead.get(ncases).getAtribuicoes()));
                System.out.println("Diferença de atribuições(Lookahead-LookaheadMVR): " + (casesLookAhead.get(ncases).getAtribuicoes() - casesLookAheadMVR.get(ncases).getAtribuicoes()));
                System.out.println("=========================");
            } else {
                //dados de saída para report
                System.out.print((ncases+1)+ ";"+tempoTotalBacktracking+ ";"+tempoTotalLookAhead+ ";"+tempoTotalLookAheadMVR+ ";");
                System.out.println(casesBackTracking.get(ncases).getAtribuicoes()+ ";"+casesLookAhead.get(ncases).getAtribuicoes()+ ";"+casesLookAheadMVR.get(ncases).getAtribuicoes()+ ";");
            }
        }
    }
}
