import java.util.HashMap;
import java.util.Vector;

//variaveis = celulas
//domainsHash = numeros de 0 a x para cada celula mapeada em um HashMap com chave construida em formataChave
//constraints = maior/menor

public class FutoshikiCaseLookAhead extends FutoshikiCaseBase{

    HashMap<String,Integer> domains;
    // ======================= Construtor e configurações iniciais ======================= //
    public FutoshikiCaseLookAhead(Vector<String> restricoes, Vector<String> linhasBoard) {
        this.linhasBoard = linhasBoard;
        parseRestricoes(restricoes);
        parseBoard(linhasBoard);
        criaDominiosParaBoard();
    }

    private void criaDominiosParaBoard() {
        domains=new HashMap<>();
        for(int nlinha=0;nlinha<linhasBoard.size();nlinha++){
            for(int ncoluna=0;ncoluna<linhasBoard.size();ncoluna++){
                for(int ndomain=1;ndomain<=linhasBoard.size();ndomain++){
                    domains.put(formataChave(nlinha,ncoluna,ndomain),ndomain);
                }
            }
        }
    }

    private String formataChave(int nlinha, int ncoluna,int ndomain) {
        return nlinha+"_"+ncoluna+"_"+ndomain;
    }

    // ======================= Funções Core ======================= //
    public boolean backtracking(int linha, int coluna, int nextDomainValue) throws Exception {
        boolean originalZero = false;
        int [] proximaPosicao = getProximaPosicao(linha,coluna);
        if(proximaPosicao==null){//solução encontrada, final da árvore
            return true;
        }
        linha = proximaPosicao[0];
        coluna = proximaPosicao[1];
        while(nextDomainValue<=linhasBoard.size()){
            int currentDomain = 0;
            if(board[linha][coluna]==0 || originalZero){
                currentDomain = getProximoDominioValido(linha,coluna,nextDomainValue);
                //System.out.println("valor dominio:"+linha+"_"+coluna+"_"+currentDomain);
                checkNumeroAtribuicoes();
                if(currentDomain<0){//todos os domínios testados, retorna
                    return false;
                }
                board[linha][coluna]=currentDomain;
                originalZero = true;
            }
            if(checkRestricoes(linha,coluna)){//se restrição ok, verifica adiante, chama recursivo, desce na árvore
                limpaPossivelDominioLinhaColuna(linha,coluna,currentDomain);
                boolean ret = backtracking(linha,coluna,1);
                if(ret){//achou a solução, sobe na árvore com solução final
                    return ret;
                } else { //Solução falhou. Reconstroi dominio para linha/coluna quando subindo/voltando na árvore
                    reconstroiPossivelDominioLinhaColuna(linha,coluna,currentDomain);
                    if(originalZero){
                        board[linha][coluna]=0; //Zera valor atual quando subindo/voltando na árvore
                    }
                }
            } else {
                if(originalZero) {
                    board[linha][coluna] = 0;//Zera valor atual pois restrição não atendida
                }
            }
            if(!originalZero){ // Se original na célula não era zero então valor já falhou
                return false;
            }
            nextDomainValue=currentDomain+1;//próximo valor a ser testado
        }
        if(originalZero){ //todos os domínios testados, restrições
            board[linha][coluna]=0;
        }
        return false;
    }


    private void reconstroiPossivelDominioLinhaColuna(int linha, int coluna, int domainValue) {
        for(int ncoluna=0;ncoluna<linhasBoard.size();ncoluna++){
            domains.put(formataChave(linha, ncoluna, domainValue),domainValue);
        }
        for(int nlinha=0;nlinha<linhasBoard.size();nlinha++){
            domains.put(formataChave(nlinha, coluna, domainValue),domainValue);
        }
    }

    private void limpaPossivelDominioLinhaColuna(int linha, int coluna, int domainValue) {
        for(int ncoluna=0;ncoluna<linhasBoard.size();ncoluna++){
            domains.remove(formataChave(linha, ncoluna, domainValue));
        }
        for(int nlinha=0;nlinha<linhasBoard.size();nlinha++){
            domains.remove(formataChave(nlinha, coluna, domainValue));
        }
    }

    private int getProximoDominioValido(int linha, int coluna, int domainValue) {
        while(!domains.containsKey(formataChave(linha,coluna,domainValue))){
            domainValue++;
            if(domainValue>linhasBoard.size()){
                return -1;//Nenhum domínio encontrado a partir do domínio
            }
        }
        return domainValue;
    }

    private int [] getProximaPosicao(int linha,int coluna){
        if(linha<0 && coluna<0){//início da árvore
            return new int[]{0,0};
        }
        coluna++;
        if(coluna>=linhasBoard.size()){
            coluna=0;
            linha++;
        }
        if(linha>=linhasBoard.size()){
            return null;
        }
        return new int[]{linha,coluna};
    }
}
