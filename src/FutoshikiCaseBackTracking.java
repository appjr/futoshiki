import java.util.Vector;

//variaveis = celulas
//domainsHash = numeros de 0 a x
//constraints = maior/menor

public class FutoshikiCaseBackTracking extends FutoshikiCaseBase{


    // ======================= Construtor e configurações iniciais ======================= //
    public FutoshikiCaseBackTracking(Vector<String> restricoes, Vector<String> linhasBoard) {
        this.linhasBoard = linhasBoard;
        parseRestricoes(restricoes);
        parseBoard(linhasBoard);
        domain = new int[linhasBoard.size()];
        for(int ndomain = 0;ndomain<linhasBoard.size();){
            domain[ndomain++]=ndomain;
        }
    }


    // ======================= Funções Core ======================= //
    public boolean backtracking(int linha, int coluna, int domainIndex) throws Exception {
        boolean originalZero = false;
        int [] proximaPosicao = getProximaPosicao(linha,coluna);
        if(proximaPosicao==null){//fim da linha, solução encontrada
            return true;
        }
        linha = proximaPosicao[0];
        coluna = proximaPosicao[1];
        while(domainIndex<board[1].length){
            if(board[linha][coluna]==0 || originalZero){
                board[linha][coluna]=domain[domainIndex];
                checkNumeroAtribuicoes();
                originalZero = true;
            }
            if(checkRestricoes(linha,coluna)){
                boolean ret = backtracking(linha,coluna,0);
                if(ret){
                    return ret;
                }
            }
            if(!originalZero){
                return false;
            }
            domainIndex++;
        }
        if(originalZero){
            board[linha][coluna]=0;
        }
        return false;
    }

    private int [] getProximaPosicao(int linha,int coluna){
        if(linha<0 && coluna<0){
            return new int[]{0,0};
        }
        coluna++;
        if(coluna>=domain.length){
            coluna=0;
            linha++;
        }
        if(linha>=domain.length){
            return null;
        }
        return new int[]{linha,coluna};
    }
}
