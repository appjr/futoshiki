import java.util.HashMap;
import java.util.Vector;

//variaveis = celulas
//domainsHash = numeros de 0 a x para cada celula mapeada em um HashMap com chave construida em formataChave
//constraints = maior/menor

public class FutoshikiCaseLookAheadMVR extends FutoshikiCaseBase{

    HashMap<String,Vector> domainsHash;
    // ======================= Construtor e configurações iniciais ======================= //
    public FutoshikiCaseLookAheadMVR(Vector<String> restricoes, Vector<String> linhasBoard) {
        this.linhasBoard = linhasBoard;
        parseRestricoes(restricoes);
        parseBoard(linhasBoard);
        criaDominiosParaBoard();
    }

    private void criaDominiosParaBoard() {
        domainsHash = new HashMap<String, Vector>();
        for(int nlinha=0;nlinha<linhasBoard.size();nlinha++){
            for(int ncoluna=0;ncoluna<linhasBoard.size();ncoluna++){
                Vector<Integer> domainsVector = new Vector<>();
                for(int ndomain=1;ndomain<=linhasBoard.size();ndomain++){
                    domainsVector.add(ndomain);
                }
                domainsHash.put(formataChave(nlinha,ncoluna),domainsVector);
            }
        }
    }

    private String formataChave(int nlinha, int ncoluna) {
        return nlinha+"_"+ncoluna;
    }

    // ======================= Funções Core ======================= //
    public boolean backtracking(int linha, int coluna, int nextDomainValue) throws Exception {
        boolean originalZero = false;
        int [] proximaPosicao = getProximaPosicaoMVR();
        if(proximaPosicao==null){//solução encontrada, final da árvore
            return true;
        }
        linha = proximaPosicao[0];
        coluna = proximaPosicao[1];
        while(nextDomainValue<=linhasBoard.size()){
            int currentDomain = 0;
            if(board[linha][coluna]==0 || originalZero){
                currentDomain = getProximoDominioValido(linha,coluna,nextDomainValue);
                checkNumeroAtribuicoes();
                if(currentDomain<0){//todos os domínios testados, retorna
                    return false;
                }
                board[linha][coluna]=currentDomain;
                originalZero = true;
            }
            if(checkRestricoes(linha,coluna)){//se restrição ok, verifica adiante, chama recursivo, desce na árvore
                limpaPossivelDominioLinhaColuna(linha,coluna,currentDomain);
                boolean ret = backtracking(linha,coluna,1);
                if(ret){//achou a solução, sobe na árvore com solução final
                    return ret;
                } else { //Solução falhou. Reconstroi dominio para linha/coluna quando subindo/voltando na árvore
                    reconstroiPossivelDominioLinhaColuna(linha,coluna,currentDomain);
                    if(originalZero){
                        board[linha][coluna]=0; //Zera valor atual quando subindo/voltando na árvore
                    }
                }
            } else {
                if(originalZero) {
                    board[linha][coluna] = 0;//Zera valor atual pois restrição não atendida
                }
            }
            if(!originalZero){ // Se original na célula não era zero então valor já falhou
                return false;
            }
            nextDomainValue=currentDomain+1;//próximo valor a ser testado
        }
        if(originalZero){ //todos os domínios testados, restrições
            board[linha][coluna]=0;
        }
        return false;
    }


    private void reconstroiPossivelDominioLinhaColuna(int linha, int coluna, int domainValue) {
        for(int ncoluna=0;ncoluna<linhasBoard.size();ncoluna++){
            Vector<Integer> domainsVector = domainsHash.get(formataChave(linha, ncoluna));
            boolean added = false;
            for(int index=0;index<domainsVector.size();index++){
                if(domainsVector.get(index)>domainValue) {
                    domainsVector.add(index, domainValue);
                    added = true;
                    break;
                }
            }
            if(!added){
                domainsVector.add(domainValue);
            }
        }
        for(int nlinha=0;nlinha<linhasBoard.size();nlinha++){
            Vector<Integer> domainsVector = domainsHash.get(formataChave(nlinha, coluna));
            boolean added = false;
            for(int index=0;index<domainsVector.size();index++){
                if(domainsVector.get(index)>domainValue) {
                    domainsVector.add(index, domainValue);
                    added = true;
                    break;
                }
            }
            if(!added){
                domainsVector.add(domainValue);
            }
        }
    }

    private void limpaPossivelDominioLinhaColuna(int linha, int coluna, int domainValue) {
        for(int ncoluna=0;ncoluna<linhasBoard.size();ncoluna++){
            Vector<Integer> domainsVector = domainsHash.get(formataChave(linha, ncoluna));
            domainsVector.removeElement(domainValue);
        }
        for(int nlinha=0;nlinha<linhasBoard.size();nlinha++){
            Vector<Integer> domainsVector = domainsHash.get(formataChave(nlinha, coluna));
            domainsVector.removeElement(domainValue);
        }
    }

    private int getProximoDominioValido(int linha, int coluna, int domainValue) {
        Vector<Integer> domainsVector = domainsHash.get(formataChave(linha, coluna));
        int domainsIndex = 0;
        int current = domainsVector.get(domainsIndex);
        for(;current < domainValue && domainsIndex < domainsVector.size();domainsIndex++ ){
            current = domainsVector.get(domainsIndex);
            if(current<domainValue){
                current=-1;
            }
        }
        return current;
    }

    private int [] getProximaPosicaoMVR() {
        int posicao[] = null;
        int nsize = linhasBoard.size()+1;
        for (int nlinha = 0; nlinha < linhasBoard.size(); nlinha++) {
            for (int ncoluna = 0; ncoluna < linhasBoard.size(); ncoluna++) {
                Vector<Integer> domainsVector = domainsHash.get(formataChave(nlinha, ncoluna));
                if (domainsVector.size() < nsize && board[nlinha][ncoluna] == 0) {
                    nsize = domainsVector.size();
                    posicao = new int[]{nlinha, ncoluna};
                }
            }
        }
        return posicao;
    }
}
