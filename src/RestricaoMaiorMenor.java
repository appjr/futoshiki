public class RestricaoMaiorMenor {
    int menorColuna;
    int menorLinha;
    int maiorColuna;
    int maiorLinha;

    public RestricaoMaiorMenor(String linhaInput){
        String [] parsedLinha = linhaInput.split(" ");
        menorLinha = Integer.parseInt(parsedLinha[0])-1;
        menorColuna = Integer.parseInt(parsedLinha[1])-1;
        maiorLinha = Integer.parseInt(parsedLinha[2])-1;
        maiorColuna = Integer.parseInt(parsedLinha[3])-1;
    }

    public boolean testaRestricao(int [][] board, int linha, int coluna){
        boolean result = true;
        if((linha == menorLinha && coluna == menorColuna) || (linha == maiorLinha && coluna == maiorColuna)){
            int menor = board[menorLinha][menorColuna];
            int maior = board[maiorLinha][maiorColuna];
            if(menor!=0 && maior!=0){
                if(menor>maior){
                    result = false; //Falso somente se condição não é satisfeita e valores diferente de zero
                }
            }
        }
        return result;
    }
}
