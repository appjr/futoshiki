public class RestricaoLinhaColuna {

    static public boolean testaRestricao(int [][] board, int linha, int coluna){
        boolean result = true;
        int nelementos = board[0].length;
        for(int nlinha=0;nlinha<nelementos;nlinha++){
            if(linha!=nlinha) {
                if (board[linha][coluna] == board[nlinha][coluna]) {
                    result = false;
                }
            }
        }
        if(result){
            for(int ncoluna=0;ncoluna<nelementos;ncoluna++){
                if(coluna!=ncoluna) {
                    if (board[linha][coluna] == board[linha][ncoluna]) {
                        result = false;
                    }
                }
            }
        }
        return result;
    }
}
