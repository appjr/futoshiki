import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class InputHelper {

    public  Vector<FutoshikiCaseBackTracking> fileToCases(String fileName){
        Vector<String> lines = loadFile(fileName);
        Vector<FutoshikiCaseBackTracking> cases = new Vector<>();
        int nline = 0;
        int ncases = Integer.parseInt(lines.get(nline++));
        for(int ncase = 0; ncase<ncases; ncase++){
            String firstCaseLine = lines.get(nline++);
            String[] caseHeader = firstCaseLine.split(" ");
            int nRestricoes = Integer.parseInt(caseHeader[1]);
            int nLinhasCase = Integer.parseInt(caseHeader[0]);
            Vector<String> restricoes = chopLines(nline,nRestricoes,lines);
            nline+=nRestricoes;
            Vector<String> linhasBoard = chopLines(nline,nLinhasCase,lines);
            nline+=nLinhasCase;
            nline++; // pula linha em branco
            cases.add(new FutoshikiCaseBackTracking(restricoes, linhasBoard));
        }
        return cases;
    }

    public  Vector<FutoshikiCaseLookAhead> fileToCasesLA(String fileName){
        Vector<String> lines = loadFile(fileName);
        Vector<FutoshikiCaseLookAhead> cases = new Vector<>();
        int nline = 0;
        int ncases = Integer.parseInt(lines.get(nline++));
        for(int ncase = 0; ncase<ncases; ncase++){
            String firstCaseLine = lines.get(nline++);
            String[] caseHeader = firstCaseLine.split(" ");
            int nRestricoes = Integer.parseInt(caseHeader[1]);
            int nLinhasCase = Integer.parseInt(caseHeader[0]);
            Vector<String> restricoes = chopLines(nline,nRestricoes,lines);
            nline+=nRestricoes;
            Vector<String> linhasBoard = chopLines(nline,nLinhasCase,lines);
            nline+=nLinhasCase;
            nline++; // pula linha em branco
            cases.add(new FutoshikiCaseLookAhead(restricoes, linhasBoard));
        }
        return cases;
    }

    public  Vector<FutoshikiCaseLookAheadMVR> fileToCasesMVR(String fileName){
        Vector<String> lines = loadFile(fileName);
        Vector<FutoshikiCaseLookAheadMVR> cases = new Vector<>();
        int nline = 0;
        int ncases = Integer.parseInt(lines.get(nline++));
        for(int ncase = 0; ncase<ncases; ncase++){
            String firstCaseLine = lines.get(nline++);
            String[] caseHeader = firstCaseLine.split(" ");
            int nRestricoes = Integer.parseInt(caseHeader[1]);
            int nLinhasCase = Integer.parseInt(caseHeader[0]);
            Vector<String> restricoes = chopLines(nline,nRestricoes,lines);
            nline+=nRestricoes;
            Vector<String> linhasBoard = chopLines(nline,nLinhasCase,lines);
            nline+=nLinhasCase;
            nline++; // pula linha em branco
            cases.add(new FutoshikiCaseLookAheadMVR(restricoes, linhasBoard));
        }
        return cases;
    }

    private Vector<String> chopLines(int currentLine, int nlines, Vector<String> lines){
        Vector<String> choppedLines = new Vector<>();
        for(int i = currentLine;i<currentLine+nlines;i++){
            choppedLines.add(lines.get(i));
        }
        return choppedLines;
    }

    private Vector<String> loadFile(String fileName) {
        Vector<String> lines = new Vector<>();
        try {
            File file = new File(getClass().getResource(fileName).getPath());
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}
